package cz.upce.fei.adt.tree

import cz.upce.fei.adt.tree.map.Coordinatable
import cz.upce.fei.adt.tree.map.area.Area
import cz.upce.fei.adt.tree.map.position.MapPosition
import org.junit.Before
import org.junit.Test
import java.util.stream.Collectors
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

internal class MapQuadTreeTest {
    private companion object{
        const val X = 25.0
        const val Y = 55.0
        const val WIDTH = 50.0
        const val HEIGHT = 70.0
        val POINTS : Map<String, Coordinates> = mapOf<String, Coordinates>(
            "z1" to Coordinates("z1", 5.0, 5.0),
            "k2" to Coordinates("k2", 10.0, 10.0),
            "u3" to Coordinates("u3", 15.0, 45.0),
            "a4" to Coordinates("a4", 20.0, 25.0),
            "w5" to Coordinates("w5", 22.0, 60.0),
            "p6" to Coordinates("p6", 25.0, 55.0),
            "r7" to Coordinates("r7", 28.0, 50.0),
            "x8" to Coordinates("x8", 30.0, 30.0),
            "n9" to Coordinates("n9", 33.0, 62.0),
            "s10" to Coordinates("s10", 35.0, 12.0),
            "f11" to Coordinates("f11", 38.0, 52.0),
            "i12" to Coordinates("i12", 40.0, 20.0),
            "t13" to Coordinates("t13", 42.0, 65.0),
            "g14" to Coordinates("g14", 45.0, 40.0),
            "m15" to Coordinates("m15", 47.0, 36.0),
        )
    }

    private var tree = MapQuadTree<Coordinates>()

    @Before
    fun setUp() {
        tree = MapQuadTree(Area(0.0, 50.0, 0.0, 70.0))
    }

    @Test
    fun toBin(){
        val expected = listOf("11", "01", "00", "00", "01", "00", "00")
        (expected.indices).forEach { assertEquals(expected[it], tree.toBin(X/WIDTH, Y/HEIGHT ,it)) }
    }

    @Test
    fun nodePosition(){
        val expected = listOf(
            MapPosition.SOUTH_EAST, MapPosition.SOUTH_WEST, MapPosition.NORTH_WEST,
            MapPosition.NORTH_WEST, MapPosition.SOUTH_WEST, MapPosition.NORTH_WEST,
            MapPosition.NORTH_WEST)

            (expected.indices).forEach { assertEquals(expected[it], tree.nodePosition(Coordinates("a", X, Y), it)) }
    }

    @Test
    fun resolutionOfTreeArea(){
        tree = MapQuadTree(POINTS.entries.stream().map{it.value}.collect(Collectors.toList()))
        assertEquals(Area(0.0, 50.0, 0.0, 70.0), tree.area)
    }

    @Test
    fun addOneToEmpty(){
        tree.build(listOf(POINTS["z1"]) as List<Coordinates>)

        assertEquals(true, tree.root.leafs[MapPosition.NORTH_WEST.treeIndex] is MapQuadTree.ValueNode<*>)
    }

    @Test
    fun addTwoToMakeItCross(){
        tree.build(listOf(POINTS["z1"], POINTS["a4"], POINTS["k2"]) as List<Coordinates>)

        var next = tree.root

        listOf(MapPosition.NORTH_WEST, MapPosition.NORTH_WEST).forEach {
            assertEquals(true, next.leafs[it.treeIndex] is MapQuadTree.MultipleNode)
            next = next.leafs[it.treeIndex]
        }
    }

    @Test
    fun addTwoToMakeItValuesOnTheEnds(){
        tree.build(listOf(POINTS["z1"], POINTS["a4"], POINTS["k2"]) as List<Coordinates>)

        val expected = mapOf(
            "z1" to listOf(MapPosition.NORTH_WEST, MapPosition.NORTH_WEST, MapPosition.NORTH_WEST),
            "a4" to listOf(MapPosition.NORTH_WEST, MapPosition.SOUTH_EAST),
            "k2" to listOf(MapPosition.NORTH_WEST, MapPosition.NORTH_WEST, MapPosition.SOUTH_EAST),
        )

        expected.keys.forEach{ point ->
            var next = tree.root
            expected[point]?.forEach {
                next = next.leafs[it.treeIndex]
            }
            assertEquals(true, next is MapQuadTree.ValueNode<*>)
        }
    }

    @Test
    fun kavickaExampleTest(){
        val expected = mapOf(
            POINTS["z1"] to listOf(MapPosition.NORTH_WEST, MapPosition.NORTH_WEST, MapPosition.NORTH_WEST),
            POINTS["k2"] to listOf(MapPosition.NORTH_WEST, MapPosition.NORTH_WEST, MapPosition.SOUTH_EAST),
            POINTS["u3"] to listOf(MapPosition.SOUTH_WEST, MapPosition.NORTH_EAST),
            POINTS["a4"] to listOf(MapPosition.NORTH_WEST, MapPosition.SOUTH_EAST),
            POINTS["w5"] to listOf(MapPosition.SOUTH_WEST, MapPosition.SOUTH_EAST),
            POINTS["p6"] to listOf(MapPosition.SOUTH_EAST, MapPosition.SOUTH_WEST, MapPosition.NORTH_WEST),
            POINTS["r7"] to listOf(MapPosition.SOUTH_EAST, MapPosition.NORTH_WEST),
            POINTS["x8"] to listOf(MapPosition.NORTH_EAST, MapPosition.SOUTH_WEST),
            POINTS["n9"] to listOf(MapPosition.SOUTH_EAST, MapPosition.SOUTH_WEST, MapPosition.SOUTH_EAST),
            POINTS["s10"] to listOf(MapPosition.NORTH_EAST, MapPosition.NORTH_WEST),
            POINTS["f11"] to listOf(MapPosition.SOUTH_EAST, MapPosition.NORTH_EAST, MapPosition.SOUTH_WEST),
            POINTS["i12"] to listOf(MapPosition.NORTH_EAST, MapPosition.SOUTH_EAST),
            POINTS["t13"] to listOf(MapPosition.SOUTH_EAST, MapPosition.SOUTH_EAST),
            POINTS["g14"] to listOf(MapPosition.SOUTH_EAST, MapPosition.NORTH_EAST, MapPosition.NORTH_EAST, MapPosition.SOUTH_WEST),
            POINTS["m15"] to listOf(MapPosition.SOUTH_EAST, MapPosition.NORTH_EAST, MapPosition.NORTH_EAST, MapPosition.NORTH_EAST)
        )
        val safeList = mutableListOf<Coordinates>()
        expected.keys.forEach { if(it != null) safeList.add(it) }
        tree.build(safeList)

        expected.keys.forEach{ point ->
            var next = tree.root
            expected[point]?.forEach {
                next = next.leafs[it.treeIndex]
            }
            assertEquals(point, (next as MapQuadTree.ValueNode<Coordinates>).emca)
        }
    }

    @Test
    fun testInclude(){
        tree.build(POINTS.entries.stream().map{it.value}.collect(Collectors.toList()))
        POINTS.entries.stream()
            .map {it.value}
            .forEach { assertEquals(true, tree.contains(it)) }
    }

    @Test
    fun kavickaAreaTest(){
        val expected = mutableListOf("r7", "p6", "n9", "w5")
        tree.build(POINTS.entries.stream().map{it.value}.collect(Collectors.toList()))
        tree.all(Coordinates(lat = 20.0, long = 40.0), Coordinates(lat = 35.0, long = 65.0))
            .forEach {
                if(expected.contains(it.name))
                    expected.remove(it.name)
                else
                    fail("Elements with name ${it.name} should not be in area searching result.")
            }
        assertTrue(expected.isEmpty())
    }

    private class Coordinates(val name: String = "", override val lat: Double, override val long: Double) : Coordinatable{
        override fun toString() = "Coordinates(name='$name')"
    }
}