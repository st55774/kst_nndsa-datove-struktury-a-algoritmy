package cz.upce.fei.io.mock

import java.nio.file.Path
import java.nio.file.Paths
import java.util.*

object MockDataSet {
    const val TEST_FILE_NAME = "blockOutPut.block"
    val TEST_DIRECTORY: Path = Paths.get(System.getProperty("java.io.tmpdir"))
    val TEST_PATH: Path = TEST_DIRECTORY.resolve(TEST_FILE_NAME)

    fun hugeSet() = (0 until 10000).map { MockCity(MockDataSet.randomString()) }.toList()

    fun randomString() : String {
        val leftLimit = 48
        val rightLimit = 122
        val targetStringLength = 32L
        val random = Random()
        val generatedString: String = random.ints(leftLimit, rightLimit + 1)
            .filter { i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97) }
            .limit(targetStringLength)
            .collect({ StringBuilder() }, java.lang.StringBuilder::appendCodePoint, java.lang.StringBuilder::append)
            .toString()
        return generatedString
    }
}