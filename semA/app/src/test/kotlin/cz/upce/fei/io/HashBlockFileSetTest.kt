package cz.upce.fei.io

import cz.upce.fei.io.file.HashBlockFileSet
import cz.upce.fei.io.mock.MockCity
import cz.upce.fei.io.mock.MockDataSet
import org.junit.Before
import org.junit.Test

import java.nio.file.Files

import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class HashBlockFileSetTest {
    private lateinit var hashBlockFileSet: HashBlockFileSet<MockCity>

    @Before
    fun beforeEach() {
        Files.deleteIfExists(MockDataSet.TEST_PATH)
        hashBlockFileSet = HashBlockFileSet(MockDataSet.TEST_PATH.toFile())
    }

    @Test
    fun saveOneElement() {
        val city = MockCity("Praha")
        hashBlockFileSet.write(city)
        assertTrue(hashBlockFileSet.contains(city))
    }

    @Test
    fun saveToOneBlock() {
        val mockCities = listOf(MockCity("A", 1), MockCity("B", 1))

        hashBlockFileSet.writeAll(mockCities)
        hashBlockFileSet.cleanCache()

        mockCities.forEach { assertTrue { hashBlockFileSet.contains(it) } }
    }

    @Test
    fun removeOneElement(){
        val city = MockCity("Praha")

        hashBlockFileSet.write(city)
        hashBlockFileSet.cleanCache()
        hashBlockFileSet.remove(city)
        hashBlockFileSet.cleanCache()

        assertFalse( hashBlockFileSet.contains(city) )
    }

    @Test
    fun removeFromOneBlock() {
        val mockCities = listOf(MockCity("A", 1), MockCity("B", 1))

        hashBlockFileSet.writeAll(mockCities)
        hashBlockFileSet.cleanCache()

        mockCities.forEach {
            hashBlockFileSet.remove(it)
            hashBlockFileSet.cleanCache()
        }

        mockCities.forEach { assertFalse { hashBlockFileSet.contains(it) } }
    }

    @Test
    fun saveToOneBlockMultipleLine() {
        val cities = (0..20).map { MockCity("$it", 1) }.toList()

        cities.forEach { hashBlockFileSet.write(it) }
        hashBlockFileSet.cleanCache()

        cities.forEach { assertTrue { hashBlockFileSet.contains(it) } }
    }

    @Test
    fun hugeDataSet() {
        val set = MockDataSet.hugeSet()

        set.forEach {
            println("$it has been write to hash file set")
            hashBlockFileSet.write(it)
        }

        set.forEach {
            if(hashBlockFileSet.contains(it)) println("$it is in hash file set")
            assertTrue { hashBlockFileSet.contains(it) }
        }
    }
}