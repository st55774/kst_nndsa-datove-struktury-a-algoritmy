package cz.upce.fei.io.mock

import cz.upce.fei.convertor.ByteConvertor
import cz.upce.fei.direction.navigation.City
import cz.upce.fei.hash.HashConvertor
import cz.upce.fei.io.Byteable

data class MockCity (
    val name : String,
    val hashCode : Int? = null) : Byteable {

    /**
     * {@inheritDoc}
     */
    override fun toByteArray(): ByteArray {
        val name = this.name.toByteArray()
        val hash = ByteConvertor.toByteArray(hashCode())
        val size = ByteConvertor.toByteArray(Int.SIZE_BYTES + name.size + hash.size)

        return size.plus(hash).plus(name)
    }

    /**
     * {@inheritDoc}
     */
    override fun toObject(array: ByteArray) : MockCity {
        val size = ByteConvertor.intFromByteArray(array.copyOfRange(SIZE_INDEX.first, SIZE_INDEX.second))
        val hash = ByteConvertor.intFromByteArray(array.copyOfRange(HASH_INDEX.first, HASH_INDEX.second))
        val name = array.copyOfRange(HASH_INDEX.second, size).decodeToString()

        return MockCity(name, hash)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MockCity

        if (name != other.name) return false
        if (hashCode() != other.hashCode()) return false

        return true
    }

    override fun hashCode(): Int = hashCode ?: HashConvertor.rotationXorHash(name).toInt()

    override fun toString() = name


    private companion object{
        val SIZE_INDEX : Pair<Int, Int> = 0 to Int.SIZE_BYTES
        val HASH_INDEX : Pair<Int, Int> = SIZE_INDEX.second to SIZE_INDEX.second + Int.SIZE_BYTES
    }
}
