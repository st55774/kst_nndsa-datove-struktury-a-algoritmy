package cz.upce.fei.adt.graph

import cz.upce.fei.adt.graph.edge.Valuable
import cz.upce.fei.adt.graph.star.ForwardStarGraph
import org.junit.Before
import org.junit.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class ForwardStarGraphTest {
    private companion object{
        const val NODE_A = "A"
        const val NODE_B = "B"
        const val NODE_C = "C"
        const val PATH : Int = 2
    }

    private class Edge(override var disable: Boolean = false) : Valuable {
        override fun toInt(): Int = PATH
    }

    private lateinit var graph: ForwardStarGraph<String, Valuable>

    @Before
    fun setUp() { graph = ForwardStarGraph() }

    @Test
    fun containsNode(){
        graph.addVertex(NODE_A)
        assertTrue(graph.containsVertex(NODE_A))
    }

    @Test
    fun notContainsNode(){
        assertFalse(graph.containsVertex(NODE_A))
    }

    @Test
    fun notContainsPath(){
        assertFalse(graph.containsEdge(NODE_A, NODE_B))
    }

    @Test
    fun addNode() {
        graph.addVertex(NODE_A)
        assertFalse(graph.empty())
    }

    @Test
    fun addPath() {
        graph.addVertex(NODE_A)
        graph.addVertex(NODE_B)
        graph.addEdge(Edge(), NODE_A, NODE_B)

        assertTrue(graph.containsEdge(NODE_A, NODE_B))
    }

    @Test
    fun addMultiple() {
        graph.addVertex(NODE_A)
        graph.addVertex(NODE_B)
        graph.addVertex(NODE_C)
        graph.addEdge(Edge(), NODE_A, NODE_B)
        graph.addEdge(Edge(), NODE_A, NODE_C)

        assertTrue(graph.containsEdge(NODE_A, NODE_B))
        assertTrue(graph.containsEdge(NODE_A, NODE_C))
    }

    @Test
    fun removeNode() {
        graph.addVertex(NODE_A)
        graph.addVertex(NODE_B)

        graph.removeVertex(NODE_A)
        assertFalse(graph.containsVertex(NODE_A))
    }

    @Test
    fun removePath() {
        graph.addVertex(NODE_A)
        graph.addVertex(NODE_B)
        graph.addEdge(Edge(), NODE_A, NODE_B)

        graph.removeEdge(NODE_A, NODE_B)
        assertFalse(graph.containsEdge(NODE_A, NODE_B))
    }
}