package cz.upce.fei.adt.tree

import cz.upce.fei.adt.tree.map.Coordinatable

/**
 * Abstract definition of K tree.
 *
 * @param <E> type of tree elements.
 * */
interface Tree<E> {
    /**
     * It will init the tree and build on the given elements.
     *
     * @param elements to be build on that tree.
     * */
    fun build(elements: List<E>)

    /**
     * Check if tree contains element.
     *
     * @param element to check.
     *
     * @return tree is contains element.
     * */
    fun contains(element: E) : Boolean

    /**
     * It will return all elements that is inside a specific area.
     *
     * @param topLeft point of the area where the tree will be searching.
     * @param bottomRight point of the area where the tree will be searching.
     *
     * @return all matched points.
     * */
    fun all(topLeft : Coordinatable, bottomRight : Coordinatable) : MutableList<E>

    /**
     * It will erase all data in tree.
     * */
    fun clean()

    /**
     * Check empty of tree.
     * */
    fun isEmpty() : Boolean

    /**
     * Provide number of elements in tree.
     *
     * @return Number of elements.
     * */
    fun size() : Int

    /**
     * Sequence of the tree.
     *
     * @return sequence of the elements.
     * */
    fun sequence(): Sequence<E>
}