package cz.upce.fei.adt.tree.map.area

import cz.upce.fei.adt.tree.map.Coordinatable

/**
 * Representing the are on map.
 *
 * @param xMin left X coordinate.
 * @param xMax right X coordinate.
 * @param yMin top Y coordinate.
 * @param yMax bottom Y coordinate.
 * */
data class Area(
    val xMin : Double = 0.0,
    val xMax : Double = 0.0,
    val yMin : Double = 0.0,
    val yMax : Double = 0.0
) {
    /**
     * Check if point is inside area.
     *
     * @param point to determinate if its inside area.
     *
     * @return true if point is inside area.
     * */
    fun isInside(point : Coordinatable) = point.lat in (xMin .. xMax) && point.long in (yMin .. yMax)

    /**
     * Determine if area is not initialized (not empty).
     *
     * @return true not initialized/empty
     * */
    fun isEmpty() = (xMin + xMax + yMin + yMax) <= 0.0

    override fun toString() : String {
        val xMin = FORMAT.format(this.xMin)
        val xMax = FORMAT.format(this.xMax)
        val yMin = FORMAT.format(this.yMin)
        val yMax = FORMAT.format(this.yMax)

        return "x=<$xMin,$xMax> y=<$yMin,$yMax>"
    }

    private companion object{
        /**
         * The digits of floating point number.
         * */
        const val DIGITS = 3

        /**
         * The string format for floating numbers.
         * */
        const val FORMAT = "%.${DIGITS}f"
    }
}
