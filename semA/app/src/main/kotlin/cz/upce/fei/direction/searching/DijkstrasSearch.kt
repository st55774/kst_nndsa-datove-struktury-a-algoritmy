package cz.upce.fei.direction.searching

import cz.upce.fei.adt.graph.Graph
import cz.upce.fei.adt.graph.edge.Valuable
import java.util.*
import java.util.stream.Collectors


/**
 * The algorithm for pricing the edges and find the most optimal from start and end point.
 * The algorithm was programmed using this video: https://www.youtube.com/watch?v=pVfj6mxhdMw
 *
 * @param <V> Symbolize vertex kind.
 * @param <E> Symbolize edge kind. Number interface is required to use toInt method (to symbolize price).
 * @param graph for the searching of the most optimal path.
 * */
class DijkstraSearch<V, E : Valuable>(private val graph: Graph<V, E>) {
    /**
     * Finding the best direction in graph.
     *
     * @param start point (or vertex).
     * @param end point (or vertex)
     *
     * @return List where indexes are the optimal direction from end point to start point.
     * */
    fun shortest(start: V, end: V, disabled: Set<E> = mutableSetOf()): Queue<E> {
        val toDiscover = PriorityQueue<Point<V>>()
        val examined = mutableSetOf<V>()
        val wayTable: MutableMap<V, Trace<V, E>> = mutableMapOf()

        toDiscover.add(Point(priority = 0, start = start))

        while (!toDiscover.isEmpty()) {
            val toExamine = toDiscover.poll()
            examined.add(toExamine.start)

            graph.getMappableEdges(toExamine.start).forEach NewRoute@{ edge ->
                if(disabled.contains(edge.data)) return@NewRoute

                val price = toExamine.priority + edge.data.toInt()

                if (!wayTable.containsKey(edge.end))
                    wayTable[edge.end] = Trace(previous = toExamine, toPrevious = edge.data)
                else if (wayTable[edge.end]!!.previous.priority > price)
                    wayTable[edge.end]!!.previous = toExamine

                if(!examined.contains(edge.end))
                    toDiscover.add(Point(priority = price, start = edge.end))
            }
        }

        val trace: Queue<E> = LinkedList()

        if(!wayTable.containsKey(end)) return trace

        var next = end
        while(next != start){
            trace.add(wayTable[next]!!.toPrevious)
            next = wayTable[next]!!.previous.start
        }

        return trace
    }

    fun findAll(start : V, end : V): MutableList<Queue<E>> {
        val best : MutableList<Queue<E>> = mutableListOf(shortest(start, end))
        if(best.first().isEmpty()) return mutableListOf()

        val disabled = best.first().stream().filter { it.disable }.collect(Collectors.toSet())
        var previous: Int

        do {
            previous = disabled.size
            val alternative = shortest(start, end, disabled)
            alternative.stream().filter {it.disable && !disabled.contains(it)}.forEach {disabled.add(it)}
            best.add(alternative)
        }while (previous != disabled.size)

        return best
    }
}