package cz.upce.fei.gui.graph

import com.brunomnsilva.smartgraph.containers.ContentZoomPane
import com.brunomnsilva.smartgraph.graph.Graph
import com.brunomnsilva.smartgraph.graphview.SmartCircularSortedPlacementStrategy
import com.brunomnsilva.smartgraph.graphview.SmartGraphPanel
import com.brunomnsilva.smartgraph.graphview.SmartGraphProperties
import com.brunomnsilva.smartgraph.graphview.SmartPlacementStrategy
import cz.upce.fei.adt.graph.star.ForwardStarGraph
import cz.upce.fei.direction.navigation.Road
import javafx.fxml.FXMLLoader

object GraphBuilder {
    private const val SMART_GRAPH_PROPERTIES = "/graph/smartgraph.properties"
    private const val SMART_GRAPH_CSS = "/graph/smartgraph.css"

    fun createSmartGraph(g: Graph<String, Road>) : SmartGraphPanel<String, Road> {
        val strategy: SmartPlacementStrategy = SmartCircularSortedPlacementStrategy()
        val properties = SmartGraphProperties(
            GraphBuilder::class.java.getResourceAsStream(SMART_GRAPH_PROPERTIES))
        val css = GraphBuilder::class.java.getResource(SMART_GRAPH_CSS).toURI()

        return SmartGraphPanel(g, properties, strategy, css)
    }

    fun createMainLayout(graphView: SmartGraphPanel<String, Road>, graphVisualise: Graph<String, Road>) : ContentZoomPane {
        val loader = FXMLLoader()

        val controller = GraphControlPanelController(graphVisualise, graphView, ForwardStarGraph())
        loader.setController(controller)

        val contentZoomPane = ContentZoomPane(graphView)
        contentZoomPane.left = loader.load(GraphBuilder::class.java.getResourceAsStream("/fxml/GraphControlPanel.fxml"))

        return contentZoomPane
    }
}