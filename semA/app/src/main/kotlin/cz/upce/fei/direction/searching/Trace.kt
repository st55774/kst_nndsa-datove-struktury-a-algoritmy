package cz.upce.fei.direction.searching

/**
 * Trace is symbolizing route to previous point.
 *
 * @param previous point in graph.
 * @param toPrevious route to previous point.
 * */
internal class Trace<V, E>(var previous:Point<V>, var toPrevious:E)