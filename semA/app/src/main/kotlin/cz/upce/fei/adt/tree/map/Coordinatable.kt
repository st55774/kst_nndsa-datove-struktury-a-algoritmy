package cz.upce.fei.adt.tree.map

/**
 * Representation point on the map by its latitude and longitude.
 * */
interface Coordinatable : Comparable<Coordinatable> {
    val lat : Double
    val long : Double
    override fun compareTo(other: Coordinatable) = (lat + long).compareTo(other.lat + other.long)
}
