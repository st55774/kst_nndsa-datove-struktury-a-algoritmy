package cz.upce.fei.adt.graph.edge

/**
 * Class representing Null value for Valuable interface.
 * */
class NoValue : Valuable {
    override fun toInt(): Int = Int.MAX_VALUE

    override var disable: Boolean
        get() = true
        set(value) = throw NotImplementedError()
}