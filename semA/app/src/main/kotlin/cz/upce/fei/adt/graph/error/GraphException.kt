package cz.upce.fei.adt.graph

/**
 * Instance of that vertex is already exists in graph.
 * */
class VertexAlreadyExistException : Throwable("Instance of vertex is already exist in graph")

/**
 * Instance of that edge is already exists in graph.
 * */
class EdgeAlreadyExistException : Throwable("Instance of edge is already exist in graph")

/**
 * Instance of that vertex is not exists in graph.
 * */
class VertexDoesntExistException : Throwable("Instance of vertex is already exist in graph")

/**
 * Instance of that edge is not exists in graph.
 * */
class EdgeDoesntExistException : Throwable("Instance of edge is already exist in graph")

/**
 * Vertex defined as starting point of edge is not include in graph.
 * */
class StartingVertexNotExistException : Throwable("Starting vertex defined as starting point of edge is not include in graph")

/**
 * Vertex defined as ending point of edge is not include in graph.
 * */
class EndingVertexNotExistException : Throwable("Ending vertex defined as ending point of edge is not include in graph")