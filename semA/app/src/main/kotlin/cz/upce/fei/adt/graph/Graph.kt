package cz.upce.fei.adt.graph

import cz.upce.fei.adt.graph.edge.Edge

/**
 * Abstract representation of graph.
 *
 * @param <V> Symbolize vertex of graph.
 * @param <E> Symbolize edge of graph. Number interface is required to use toInt method (to symbolize price).
 *
 * */
interface Graph<V, E> {
    /**
     * Checking empty graph.
     *
     * @return true if graph is empty.
     * */
    fun empty():Boolean

    /**
     * Add vertex do graph.
     *
     * @param node to include.
     * */
    fun addVertex(node:V)

    /**
     * Add edge to graph.
     *
     * @param start vertex of edge.
     * @param end vertex of edge.
     *
     * @return Instance of path which is include of graph.
     * */
    fun addEdge(edge:E, start:V, end:V)

    /**
     * Checking if vertex is part of graph.
     *
     * @param vertex check if vertex is part of graph.
     *
     * @return true if vertex is part of graph.
     * */
    fun containsVertex(vertex:V) : Boolean

    /**
     * Checking if edge is available in graph.
     *
     * @param start vertex of edge.
     * @param end vertex of edge.
     *
     * @return true if edge is part of graph.
     * */
    fun containsEdge(start:V, end:V) : Boolean

    /**
     * Remove Vertex from graph.
     *
     * @param vertex to remove from graph.
     * */
    fun removeVertex(vertex:V)

    /**
     * Remove Edge from graph.
     *
     * @param<V> start vertex of edge.
     * @param<V> end vertex of edge.
     * */
    fun removeEdge(start:V, end:V)

    /**
     * Return all vertexes from graph.
     *
     * @return<V> set of all vertexes to easy access.
     * */
    fun vertexes(): MutableSet<V>

    /**
     * Return all edges from graph.
     *
     * @param start all edges stared by vertex.
     *
     * @return set of all edges to easy access.
     * */
    fun getMappableEdges(start:V) : MutableSet<Edge<V, E>>

    /**
     * Return all edges from graph.
     *
     * @return set of all edges to easy access.
     * */
    fun edges() :Iterator<E>

    /**
     * Clean all graph data.
     * */
    fun clean()

    /**
     * Return the edge.
     *
     * @param<V> start vertex of edge.
     * @param<V> end vertex of edge.
     *
     * @return<E> edge between two vertexes.
     * */
    fun getEdge(start: V, end: V) : E?
}

