package cz.upce.fei.service

import cz.upce.fei.direction.navigation.City
import cz.upce.fei.direction.navigation.Road
import java.io.*
import java.util.*
import java.util.regex.Pattern

/**
 * Service for loading graph from files.
 * */
object FileGraphService {
    private const val REGEX = "([a-zA-Z0-9]+)&([a-zA-Z0-9]+):([0-9]+)(\\*)?"
    private const val CORD_REGEX = "([a-zA-Z0-9]+):X=([0-9]+.[0-9]+)Y=([0-9]+.[0-9]+)"
    private const val SEPARATOR = "-------------------------------"
    private const val COORDINATES_SEPARATOR = "PATHS"

    /**
     * It will load area content.
     *
     * @param path to file.
     *
     * @return pair of cities and roads.
     * */
    fun load(path: String) : Pair<MutableList<City>, MutableList<Road>>{
        val reader = BufferedReader(FileReader(path))
        val cities = loadCities(reader)
        val paths = loadPaths(reader)

        return Pair(cities, paths)
    }

    /**
     * It will load roads from file.
     *
     * @param reader of file.
     *
     * @return list of all roads.
     * */
    private fun loadCities(reader : BufferedReader) : MutableList<City> {
        val cities = mutableListOf<City>()

        while(reader.ready()){
            val line = reader.readLine()
            if(line == COORDINATES_SEPARATOR) break

            val pattern = Pattern.compile(CORD_REGEX)
            val matcher = pattern.matcher(line)

            while (matcher.find()) {
                cities.add(City(
                    matcher.group(1),
                    matcher.group(2).toDouble(),
                    matcher.group(3).toDouble()
                ))
            }
        }

        return cities
    }

    /**
     * It will load roads from file.
     *
     * @param reader of file.
     *
     * @return list of all roads.
     * */
    private fun loadPaths(reader : BufferedReader): MutableList<Road> {
        val paths = mutableListOf<Road>()

        while(reader.ready()) {
            val it = reader.readLine()

            val pattern = Pattern.compile(REGEX)
            val matcher = pattern.matcher(it)

            while (matcher.find()) {
                val road = Road(
                    matcher.group(3).toInt(),
                    City(matcher.group(1)),
                    City(matcher.group(2))
                )
                if ("*".equals(matcher.group(4)))
                    road.disable = true
                paths.add(road)
            }
        }

        return paths
    }

    /**
     * It will save traces to file.
     *
     * @param file to save.
     * @param traces the traces
     * */
    fun saveToFile(file: File, traces: MutableList<Queue<Road>>) {
        val writer = BufferedWriter(FileWriter(file))

        traces.forEach { directionTraces ->
            directionTraces.forEach {
                writer.write(it.toString())
                writer.newLine()
            }
            writer.write(SEPARATOR)
            writer.newLine()
        }

        writer.close()
    }
}
