package cz.upce.fei.io.file

import cz.upce.fei.io.Byteable
import java.io.*

/**
 * The block input output file reader.
 *
 * @param file to save blocks.
 * */
class HashBlockFileSet<T : Byteable>(file: File) : FileInputOutput<T> {
    /**
     * The stream to place and read blocks.
     * */
    private val stream = RandomAccessFile(file, "rw")

    /**
     * Cache for speed accessing blocks.
     * */
    private var cache: Pair<Long, MutableList<ByteArray>> = INIT_CACHE

    /**
     * Indicates number of initialized lines
     * */
    private var lines = 0

    init {
        initLine(lines)
    }

    /**
     * {@inheritDoc}
     */
    override fun writeAll(items: List<T>) = items.forEach { write(it) }

    /**
     * {@inheritDoc}
     */
    override fun write(item: T) {
        val index = calculateTableHash(item)

        if (index != cache.first) loadBlock(index)
        write(item, index)
    }

    /**
     * Calculate hash table index.
     *
     * @param item which his hash code will be calculated.
     * */
    @OptIn(ExperimentalUnsignedTypes::class)
    private fun calculateTableHash(item: T) = (item.hashCode().toULong() % HASH_TABLE_SIZE.toULong()).toLong()

    /**
     * {@inheritDoc}
     */
    override fun all(value: T): List<T> {
        val all = mutableListOf<T>()

        (0 until HASH_TABLE_SIZE).forEach { block ->
            loadBlock(block.toLong())
            all.addAll(cache.second.filter { it.sum() != 0 }
                .map { value.toObject(it) }
                .map { it as T }
                .toList()
            )
        }
        return all
    }

    /**
     * Write element in byte array to file.
     *
     * @param item to write.
     * @param blockIndex to write item.
     * */
    private fun write(item: T, blockIndex: Long) {
        val cellIndex = putToCache(item.toByteArray())
        val line = calculateLine(cellIndex)

        val pointer = calculatePointer(blockIndex, cellIndex, line)
        write(pointer, item.toByteArray(), if(checkFullness(cellIndex)) FULL else FREE)
    }

    /**
     * Write element in byte array to file.
     *
     * @param array to write.
     * @param pointer to file.
     * */
    private fun write(pointer: Long, array: ByteArray = ByteArray(CELL_SIZE), state : Byte = FREE) {
        stream.seek(pointer)
        stream.write(array)

        if(state == FULL) {
            stream.seek(pointer + BLOCK_SIZE - 1)
            stream.write(byteArrayOf(FULL))
        }
    }

    /**
     * Calculate the pointer in file where element will be placed.
     *
     * @param blockIndex where element will be placed.
     * @param cellIndex where element will be placed.
     * @param line where element will be placed.
     *
     * @return pointer to file.
     * */
    private fun calculatePointer(blockIndex: Long, cellIndex: Int, line: Int = 0): Long {
        val calcIndex = cellIndex - (line * BLOCK_LENGTH)
        val next = (line * LINE_SIZE) + (blockIndex * BLOCK_SIZE) + (calcIndex * CELL_SIZE)

        if (next < lines) lines = next.toInt()
        return next
    }

    /**
     * Check if block is full.
     *
     * @param cellIndex index of cell where new element will be placed.
     *
     * @return true if block is full.
     * */
    private fun checkFullness(cellIndex : Int) = (cellIndex + 1) % BLOCK_LENGTH == 0

    /**
     * Calculate the line where element will be placed.
     *
     * @param blockSize the size of block where element will be placed.
     *
     * @return the line where element will be placed.
     * */
    private fun calculateLine(blockSize: Int) : Int{
        val line = blockSize / BLOCK_LENGTH

        if(line > lines) lines = line

        return line
    }

    /**
     * {@inheritDoc}
     */
    override fun contains(value: T): Boolean {
        val hashCode = calculateTableHash(value)

        return if (hashCode == cache.first) {
            val empty = findInCache(value)
            empty != null
        } else {
            loadBlock(hashCode)
            contains(value)
        }
    }

    /**
     * {@inheritDoc}
     */
    override fun remove(value: T) {
        val indexBlock = calculateTableHash(value)

        if (indexBlock != cache.first) loadBlock(indexBlock)

        if (contains(value)) {
            val indexCell = removeFromCache(value)
            write(calculatePointer(indexBlock, indexCell, calculateLine(indexCell)))
        }
    }

    /**
     * Remove value from cache.
     *
     * @param value to remove.
     *
     * @return index from cache.
     * */
    private fun removeFromCache(value: T): Int {
        val index = cache.second.indices
            .filter { cache.second[it].sum() != 0 }
            .firstOrNull { value.toObject(cache.second[it]) == value }

        return if (index != null) {
            cache.second[index] = ByteArray(CELL_SIZE)
            index
        } else -1
    }

    /**
     * {@inheritDoc}
     */
    override fun close() = stream.close()

    /**
     * Put element into loaded cache block.
     *
     * @return index of cell where the elements was placed.
     * */
    private fun putToCache(array: ByteArray): Int {
        var index = cache.second.indices.firstOrNull { cache.second[it].sum() == 0 }

        if (index != null)
            cache.second[index] = array
        else {
            cache.second.add(array)
            index = cache.second.lastIndex
        }

        return index
    }

    /**
     * Load block from file.
     *
     * @param index loading block.
     * */
    private fun loadBlock(index: Long) {
        var byteArray = ByteArray(0)
        var count = 0

        (0..lines).forEach {
            val array = ByteArray(BLOCK_SIZE)

            stream.seek((index * BLOCK_SIZE) + (it * LINE_SIZE))
            stream.read(array)

            byteArray = byteArray.plus(array)
            count++

            if(array.last() == FREE)
                return@forEach
        }

        cache = index to separateBlocks(byteArray, count)
    }

    /**
     * It will separate full block into multiple small cells.
     *
     * @return cells of block.
     * */
    private fun separateBlocks(block: ByteArray, count: Int): MutableList<ByteArray> {
        val blocks = mutableListOf<ByteArray>()
        (0 until (count * BLOCK_LENGTH)).forEach {
            blocks.add(it, block.copyOfRange(it * CELL_SIZE, (it + 1) * CELL_SIZE))
        }
        return blocks
    }

    /**
     * It will find the element in cache (loaded block).
     *
     * @return true if object is include in block.
     * */
    private fun findInCache(value: T) = cache.second
        .filter { !(it.sum() == 0 || (it.sum() == 1 && it.last() == FULL)) }
        .map { value.toObject(it) }
        .firstOrNull { it == value }

    /**
     * It will fill the new line with empty bytes.
     *
     * @param line number.
     * @param amount of zeros to put inside the line.
     * */
    private fun initLine(line: Int, amount: Long = (BLOCK_SIZE.toLong() * HASH_TABLE_SIZE)) {
        stream.seek(line * amount)
        stream.write(ByteArray(amount.toInt()))
    }

    /**
     * Reset the cache.
     * */
    fun cleanCache() { cache =  INIT_CACHE }

    private companion object {
        /**
         * Hast table size.
         * */
        const val HASH_TABLE_SIZE = 64
        /**
         * Cell size in bytes.
         * */
        const val CELL_SIZE = 128

        /**
         * The count of cells in one block.
         * */
        const val BLOCK_LENGTH = 11

        /**
         * Block size in bytes.
         * */
        const val BLOCK_SIZE = CELL_SIZE * BLOCK_LENGTH

        /**
         * Line size in bytes.
         * */
        const val LINE_SIZE = BLOCK_SIZE * HASH_TABLE_SIZE
        /**
         * Initialization cache
         * */
        val INIT_CACHE = -1L to mutableListOf(ByteArray(0))
        /**
         * Indicates full block.
         * */
        const val FULL : Byte = 1
        /**
        *  Indicates free block.
        * */
        const val FREE : Byte = 0
    }
}