package cz.upce.fei.adt.tree.map.position

enum class MapPosition(val bin : String, val treeIndex : Int, treeLeaf : TreeLeaf) {
    NORTH_WEST("00", 0, TreeLeaf.LEFT_UPPER),
    NORTH_EAST("10", 1, TreeLeaf.LEFT_MIDDLE),
    SOUTH_EAST("11", 2, TreeLeaf.RIGHT_UPPER),
    SOUTH_WEST("01", 3, TreeLeaf.RIGHT_MIDDLE)
}