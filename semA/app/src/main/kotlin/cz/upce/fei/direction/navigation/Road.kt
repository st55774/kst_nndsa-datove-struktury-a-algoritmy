package cz.upce.fei.direction.navigation

import cz.upce.fei.adt.graph.edge.Valuable

/**
 * Road between two points.
 *
 * @param length in kilometers.
 * @param start city on road.
 * @param end city on road.
 * @param disable road between the cities.
 * */
class Road(private val length:Int, val start:City, val end:City, override var disable: Boolean = false) : Valuable {
    override fun toInt(): Int = length
    override fun toString(): String = "$start&$end:$length"
    fun toStringReverse(): String = "$end&$start:$length"
}
