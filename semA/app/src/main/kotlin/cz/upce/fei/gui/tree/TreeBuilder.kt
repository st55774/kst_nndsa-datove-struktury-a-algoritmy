package cz.upce.fei.gui.tree

import cz.upce.fei.adt.tree.MapQuadTree
import cz.upce.fei.direction.navigation.City
import javafx.fxml.FXMLLoader
import javafx.scene.Parent

object TreeBuilder {
    fun createMainLayout(tree: List<City>) : Parent {
        val loader = FXMLLoader()
        loader.setController(TreeControlPanelController(MapQuadTree(tree)))
        return loader.load(TreeBuilder::class.java.getResourceAsStream("/fxml/TreeControlPanel.fxml"))
    }
}