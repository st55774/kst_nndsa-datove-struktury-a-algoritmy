package cz.upce.fei.hash

object HashConvertor {
    private const val LEFT_MOVE = 4
    private const val RIGHT_MOVE = 28

    @OptIn(ExperimentalUnsignedTypes::class)
    fun rotationXorHash(key: String) : ULong {
        var hash : ULong = key.length.toULong()
        key.chars().forEach { hash = ((hash shl LEFT_MOVE) xor (hash shr RIGHT_MOVE) xor it.toULong()) }
        return hash
    }
}