package cz.upce.fei.adt.tree.map.position

enum class TreeLeaf{
    LEFT_UPPER,
    LEFT_MIDDLE,
    RIGHT_MIDDLE,
    RIGHT_UPPER
}