package cz.upce.fei.direction.navigation

import cz.upce.fei.adt.tree.map.Coordinatable
import cz.upce.fei.convertor.ByteConvertor
import cz.upce.fei.io.Byteable
import cz.upce.fei.hash.HashConvertor

/**
 * City representing point.
 *
 * @param name of the city.
 * */
class City(
    open val name:String,
    override val lat : Double = 0.0,
    override val long : Double = 0.0) : Coordinatable, Byteable {

    /**
     * {@inheritDoc}
     */
    override fun toByteArray(): ByteArray {
        val name = this.name.toByteArray()
        val lat = ByteConvertor.toByteArray(this.lat)
        val long = ByteConvertor.toByteArray(this.long)
        val size = ByteConvertor.toByteArray(name.size + lat.size + long.size)

        return size.plus(lat).plus(long).plus(name)
    }

    /**
     * {@inheritDoc}
     */
    override fun toObject(array: ByteArray) : City {
        val size = ByteConvertor.intFromByteArray(array.copyOfRange(SIZE_INDEX.first, SIZE_INDEX.second))
        val lat = ByteConvertor.doubleFromByteArray(array.copyOfRange(LAT_INDEX.first, LAT_INDEX.second))
        val long = ByteConvertor.doubleFromByteArray(array.copyOfRange(LONG_INDEX.first, LONG_INDEX.second))
        val name = array.copyOfRange(LONG_INDEX.second, size + SIZE_INDEX.second).decodeToString()

        return City(name, lat, long)
    }

    /**
     * {@inheritDoc}
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as City

        if (name != other.name) return false

        return true
    }

    /**
     * {@inheritDoc}
     */
    override fun hashCode(): Int {
        val rotationXorHash = HashConvertor.rotationXorHash(name)
        return rotationXorHash.toInt()
    }

    /**
     * {@inheritDoc}
     */
    override fun toString() = "$name (x: $lat y: $long)"

    private companion object{
        val SIZE_INDEX : Pair<Int, Int> = 0 to Int.SIZE_BYTES
        val LAT_INDEX : Pair<Int, Int> = SIZE_INDEX.second to SIZE_INDEX.second + Double.SIZE_BYTES
        val LONG_INDEX : Pair<Int, Int> = LAT_INDEX.second to LAT_INDEX.second + Double.SIZE_BYTES
    }
}