package cz.upce.fei.io

/**
 * Interface for object to convert them to byte array.
 * */
interface Byteable {
    /**
     * Convert object to byte array.
     *
     * @return byte array representation of the object.
     * */
    fun toByteArray() : ByteArray
    /**
     * Covert byte array to object.
     *
     * @param array to convert.
     *
     * @return object representation.
     * */
    fun toObject(array : ByteArray) : Byteable
}