package cz.upce.fei

import com.brunomnsilva.smartgraph.graph.Graph
import com.brunomnsilva.smartgraph.graph.GraphEdgeList
import cz.upce.fei.direction.navigation.Road
import cz.upce.fei.gui.graph.GraphBuilder
import cz.upce.fei.gui.tree.TreeBuilder
import javafx.application.Application
import javafx.scene.Scene
import javafx.stage.Stage

const val MAIN_WINDOW_TITLE = "Sem A: Road graph application"
const val WINDOW_WIDTH = 1024.0
const val WINDOW_HEIGHT = 768.0

fun main() {
    Application.launch(RoadGraphApplication::class.java)
}

class RoadGraphApplication : Application() {
    companion object{
        var primaryStage : Stage = Stage()
    }

    override fun start(primaryStage: Stage) {
        RoadGraphApplication.primaryStage = primaryStage
        RoadGraphApplication.primaryStage.title = MAIN_WINDOW_TITLE

        val graphVisualise: Graph<String, Road> = GraphEdgeList()
        val smartGraph = GraphBuilder.createSmartGraph(graphVisualise)
        smartGraph.setAutomaticLayout(true)

        val mainLayout = GraphBuilder.createMainLayout(smartGraph, graphVisualise)

        primaryStage.scene = Scene(mainLayout, WINDOW_WIDTH, WINDOW_HEIGHT)
        primaryStage.show()
        smartGraph.init()
    }
}

