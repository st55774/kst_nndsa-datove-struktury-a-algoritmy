package cz.upce.fei.adt.graph.star

import cz.upce.fei.adt.graph.*
import cz.upce.fei.adt.graph.edge.Edge
import cz.upce.fei.adt.graph.edge.NoValue
import java.util.*

/**
 * The implementation of graph using forward star.
 * The graph support two way edges.
 *
 * @param <V> Symbolize vertex of graph.
 * @param <E> Symbolize edge of graph. Valuable interface is required to use toInt method (to symbolize price).
 *
 * */
class ForwardStarGraph<V, E> : Graph<V, E> {
    /**
     * The graph is using HastMap where key is vertexes of graph and value of every key is set of edges.
     * Reason of using hashmap is to quickly access the edges of vertex when the most optimal path is searching.
     * Reason of using hashset of edges is to quickly check if vertex contains the edge.
     * */
    private val star:MutableMap<V, MutableSet<Edge<V, E>>> = Hashtable()

    override fun empty() = star.isEmpty()

    override fun containsVertex(vertex: V) : Boolean = star.containsKey(vertex)

    override fun containsEdge(start:V, end:V) : Boolean =
            if(star.contains(start)) star[start]!!.any { it.end == end } else false

    override fun addVertex(node: V) {
        if(star.containsKey(node))
            throw VertexAlreadyExistException()
        star[node] = mutableSetOf()
    }

    override fun addEdge(edge: E, start: V, end: V) {
        if(!star.containsKey(start)) throw StartingVertexNotExistException()
        if(!star.containsKey(end)) throw EndingVertexNotExistException()
        if(containsEdge(start, end)) throw EdgeAlreadyExistException()

        val element = Edge(edge, start = start, end = end)
        val elementReverse = Edge(edge, start = end, end = start)

        star[start]?.add(element)
        star[end]?.add(elementReverse)
    }

    override fun removeVertex(vertex: V){
        if(!star.containsKey(vertex))
            throw VertexDoesntExistException()
        star.remove(vertex)
    }

    override fun removeEdge(start:V, end:V) {
        if(!star.containsKey(start)) throw StartingVertexNotExistException()
        if(!star.containsKey(end)) throw EndingVertexNotExistException()
        if(!containsEdge(start, end)) throw EdgeDoesntExistException()

        star[start]?.remove(Edge(NoValue(), start, end))
        star[end]?.remove(Edge(NoValue(), end, start))
    }

    override fun vertexes(): MutableSet<V> = star.keys

    override fun getMappableEdges(start: V) : MutableSet<Edge<V, E>> = if(star.contains(start)) star[start]!! else mutableSetOf()

    override fun edges(): Iterator<E> {
        val edges = mutableListOf<E>()
        star.keys.forEach { vertex -> star[vertex]?.forEach {
              edges.add(it.data)
        }}
        return edges.iterator()
    }

    override fun clean() = star.clear()

    override fun getEdge(start: V, end: V): E? {
        var edge : E? = null
        star[start]!!.forEach {
            if(it.end == end) edge = it.data
        }

        return edge
    }


}
