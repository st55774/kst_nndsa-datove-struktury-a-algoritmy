package cz.upce.fei.convertor

import java.io.*

object ByteConvertor {
    fun toByteArray(i: Double): ByteArray {
        val bos = ByteArrayOutputStream()
        val dos = DataOutputStream(bos)
        dos.writeDouble(i)
        dos.flush()
        return bos.toByteArray()
    }

    fun toByteArray(i: Int): ByteArray {
        val bos = ByteArrayOutputStream()
        val dos = DataOutputStream(bos)
        dos.writeInt(i)
        dos.flush()
        return bos.toByteArray()
    }

    fun intFromByteArray(i: ByteArray): Int {
        val bos = ByteArrayInputStream(i)
        val dos = DataInputStream(bos)
        return dos.readInt()
    }

    fun doubleFromByteArray(i: ByteArray): Double {
        val bos = ByteArrayInputStream(i)
        val dos = DataInputStream(bos)
        return dos.readDouble()
    }
}