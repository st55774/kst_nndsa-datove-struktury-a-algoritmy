package cz.upce.fei.gui.tree

import cz.upce.fei.adt.tree.MapQuadTree
import cz.upce.fei.direction.navigation.City
import cz.upce.fei.gui.GuiAlerts
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import java.net.URL
import java.util.*

class TreeControlPanelController(val tree: MapQuadTree<City> = MapQuadTree()) : Initializable{
    @FXML
    private lateinit var cityName: TextField
    @FXML
    private lateinit var xFind: TextField
    @FXML
    private lateinit var yFind: TextField
    @FXML
    private lateinit var Xmin: TextField
    @FXML
    private lateinit var Xmax: TextField
    @FXML
    private lateinit var Ymin: TextField
    @FXML
    private lateinit var Ymax: TextField
    @FXML
    private lateinit var result: TextArea
    @FXML
    private lateinit var main : TreeView<MapQuadTree.Node>

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        main.root = createTreeView(tree.root)
    }

    @FXML
    fun onCityFindButtonClicked(actionEvent: ActionEvent) {
        GuiAlerts.containsCity(tree.contains(City(
            cityName.text,
            xFind.text.toDouble(),
            yFind.text.toDouble()
        )))
    }

    @FXML
    fun onCityAreaFindButtonClicked(actionEvent: ActionEvent) {
        var result = ""
        tree.all(
            SearchCoordinates(Xmin.text.toDouble(), Ymin.text.toDouble()),
            SearchCoordinates(Xmax.text.toDouble(), Ymax.text.toDouble())
        ).forEach { result += ("${it.name} ") }

        this.result.text = result
    }

    private companion object{
        /**
         * Build a structure based on quadtree for JavaFX TreeView.
         *
         * @param root of the quad tree.
         *
         * @return the Java FX TreeItem structure based on original quad tree structure.
         * */
        fun createTreeView(root : MapQuadTree.Node) : TreeItem<MapQuadTree.Node>{
            val queue : Queue<Pair<MapQuadTree.Node, TreeItem<MapQuadTree.Node>>> = LinkedList()
            val main = TreeItem(root)

            root.leafs.forEach { queue.add(it to main) }

            while(!queue.isEmpty())
                queue.addAll(createChildItems(queue.poll()))

            return main
        }

        /**
         * It will append the node to the parent tree.
         *
         * @param toAdd node to be appended to tree.
         *
         * @return the childs of node.
         * */
        private fun createChildItems(toAdd : Pair<MapQuadTree.Node, TreeItem<MapQuadTree.Node>>):
                List<Pair<MapQuadTree.Node, TreeItem<MapQuadTree.Node>>> {

            val node = toAdd.first
            val parentItem = toAdd.second
            val childItem = TreeItem(node)

            parentItem.children.add(childItem)
            return node.leafs.map { it to childItem }.toList()
        }
    }
}