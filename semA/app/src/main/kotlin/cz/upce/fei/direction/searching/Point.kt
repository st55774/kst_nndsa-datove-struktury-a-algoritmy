package cz.upce.fei.direction.searching

/**
 * Point in the graph which will be examinated in next step od Dijsktra searching algorithm.
 *
 * @param priority of this point in next step of searching algorithm.
 * @param start point to examinate the routes.
 * */
internal class Point<V>(var priority:Int, var start:V) : Comparable<Point<V>>{
    override fun compareTo(other: Point<V>): Int = priority.compareTo(other.priority)
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Point<*>

        if (start != other.start) return false

        return true
    }

    override fun hashCode(): Int {
        return start?.hashCode() ?: 0
    }
}