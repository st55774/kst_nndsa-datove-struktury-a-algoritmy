package cz.upce.fei.adt.graph.edge

/**
 * To determine value of Edge.
 *
 * */
interface Valuable {
    /**
     * To determine value of edge.
     *
     * @return number value of edge.
     * */
    fun toInt() : Int

    /**
     * Define if the edge is disabled.
     * */
    var disable : Boolean
}

