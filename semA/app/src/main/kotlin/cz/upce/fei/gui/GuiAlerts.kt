package cz.upce.fei.gui

import javafx.scene.control.Alert

object GuiAlerts {
    private const val ERROR_TEXT = "Zkuste to znovu"

    /**
     * It will show input error window.
     * */
    fun input() {
        val alert = Alert(Alert.AlertType.ERROR)
        alert.title = "Chybný vstup"
        alert.headerText = "Jeden ze vstupů není čislo."
        alert.contentText = ERROR_TEXT

        alert.showAndWait()
    }

    fun <V>notExistingVertex(city:V, start:Boolean){
        val alert = Alert(Alert.AlertType.ERROR)
        alert.title = "Neexistující ${if(start)"počáteční" else "koncová"}"
        alert.headerText = "Obec $city neexistuje."
        alert.contentText = ERROR_TEXT

        alert.showAndWait()
    }

    fun <V>roadAlreadyExist(start:V, end:V){
        val alert = Alert(Alert.AlertType.ERROR)
        alert.title = "Cesta mezi danými obecmi již existuje"
        alert.headerText = "Cesta mezi $start a $end je již definovaná"
        alert.contentText = ERROR_TEXT

        alert.showAndWait()
    }

    fun <V>fileCannotBeOpened(){
        val alert = Alert(Alert.AlertType.ERROR)
        alert.title = "Chyba vstupu."
        alert.headerText = "Nelze otevřít soubor"
        alert.contentText = ERROR_TEXT

        alert.showAndWait()
    }

    fun <V>noTraceFound(start: V, end: V){
        val alert = Alert(Alert.AlertType.INFORMATION)
        alert.title = "Žádná dostupná trasa"
        alert.headerText = "Mezi $start a $end nebyla nalezena žádná trasa."
        alert.contentText = ERROR_TEXT

        alert.showAndWait()
    }

    fun containsCity(contains : Boolean){
        val alert = Alert(if(contains) Alert.AlertType.INFORMATION else Alert.AlertType.WARNING)
        alert.title = if(contains) "Obsahuje" else "Neobsahuje"
        alert.headerText = if(contains) "Obsahuje" else "Neobsahuje"

        alert.showAndWait()
    }


}