package cz.upce.fei.adt.tree.exception

class MaxLevelOfTreeReached : Throwable()