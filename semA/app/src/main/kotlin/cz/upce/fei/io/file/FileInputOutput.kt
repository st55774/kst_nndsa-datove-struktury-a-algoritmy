package cz.upce.fei.io.file

import cz.upce.fei.io.Byteable

/**
 * Stream to read a write to file.
 *
 * @param T type of files.
 * */
interface FileInputOutput<T : Byteable> {
    /**
     * Write all items to file.
     *
     * @param items to save.
     * */
    fun writeAll(items: List<T>)

    /**
     * Write item to file.
     *
     * @param item to save.
     * */
    fun write(item: T)

    /**
     * Remove element from file
     *
     * @param value to remove from file.
     * */
    fun remove(value : T)

    /**
     * Check if file contains value.
     *
     * @param value to check.
     *
     * @return true if file contains value. False if not.
     * */
    fun contains(value: T): Boolean

    /**
     * Collect all elements in the file.
     *
     * @param value to convert te byte array to object.
     *
     * @return all elements.
     * */
    fun all(value: T): List<T>

    /**
     * Close the file stream.
     * */
    fun close()
}