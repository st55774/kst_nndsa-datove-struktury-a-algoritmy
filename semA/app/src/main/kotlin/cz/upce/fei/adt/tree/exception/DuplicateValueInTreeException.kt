package cz.upce.fei.adt.tree.exception

class DuplicateValueInTreeException : Throwable()