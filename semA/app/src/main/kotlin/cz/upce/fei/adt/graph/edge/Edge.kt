package cz.upce.fei.adt.graph.edge

/**
 * Symbolizing edge between two vertexes.
 *
 * @param data data.
 * @param start vertex of edge.
 * @param end vertex of edge.
 *
 * @param <V> Symbolize vertex of graph.
 * @param <E> Symbolize edge of graph. Valuable interface is required to use toInt method (to symbolize price).
 * */
class Edge<V, E>(val data:E, val start:V, val end:V){
    override fun toString(): String = "$start&$end:$data"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Edge<*, *>

        if (start != other.start) return false
        if (end != other.end) return false

        return true
    }

    override fun hashCode(): Int {
        var result = start?.hashCode() ?: 0
        result = 31 * result + (end?.hashCode() ?: 0)
        return result
    }
}