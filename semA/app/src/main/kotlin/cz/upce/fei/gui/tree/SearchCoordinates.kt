package cz.upce.fei.gui.tree

import cz.upce.fei.adt.tree.map.Coordinatable

data class SearchCoordinates(override val lat: Double, override val long: Double) : Coordinatable
