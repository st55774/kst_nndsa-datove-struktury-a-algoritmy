package cz.upce.fei.gui.graph

import com.brunomnsilva.smartgraph.graph.Graph
import com.brunomnsilva.smartgraph.graphview.SmartGraphPanel
import cz.upce.fei.RoadGraphApplication
import cz.upce.fei.adt.graph.star.ForwardStarGraph
import cz.upce.fei.direction.navigation.City
import cz.upce.fei.direction.navigation.Road
import cz.upce.fei.direction.searching.DijkstraSearch
import cz.upce.fei.gui.GuiAlerts
import cz.upce.fei.gui.tree.TreeBuilder
import cz.upce.fei.service.FileGraphService
import javafx.application.Platform
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.Scene
import javafx.scene.control.CheckBox
import javafx.scene.control.ComboBox
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.stage.FileChooser
import javafx.stage.Stage
import java.net.URL
import java.util.*


/**
 * Controller of left side control panel.
 *
 * @param fxmlGraphModel Graph by third party. THIS IS NOT ADT made by author.
 * @param fxmlGraphPanel Canvas for drawing graph.
 * @param graph ADT created by author.
 * */
class GraphControlPanelController(
        private val fxmlGraphModel: Graph<String, Road>,
        private val fxmlGraphPanel: SmartGraphPanel<String, Road>,
        private val graph: ForwardStarGraph<City, Road>) : Initializable {

    private val search : DijkstraSearch<City, Road> = DijkstraSearch(graph)

    private companion object {
        val COLORS : List<String> = listOf(
                "edge-label-green", "edge-label-blue", "edge-label-red"
        )
        const val FIND_VERTEX_COLOR = "vertex-find"
        const val FIND_EDGE_COLOR = "locatedVertex"
    }

    /**
     * Set of rendered Edges.
     * */
    private val readyEdges = mutableSetOf<String>()

    /**
     * Set of rendered Vertexes.
     * */
    private val readyNodes = mutableSetOf<String>()

    @FXML
    private lateinit var AddPathSource: TextField

    @FXML
    private lateinit var AddPathTarget: TextField

    @FXML
    private lateinit var AddPathPrice: TextField

    @FXML
    private lateinit var DisablePathSource: TextField

    @FXML
    private lateinit var DisablePathTarget: TextField

    @FXML
    private lateinit var OptimalPathSource: TextField

    @FXML
    private lateinit var OptimalPathTarget: TextField

    @FXML
    private lateinit var OptimalPaths: TextArea

    @FXML
    private lateinit var SaveToFile: CheckBox

    @FXML
    private lateinit var BestTraces : ComboBox<Queue<Road>>

    @FXML
    private lateinit var ExistPathSource : TextField

    @FXML
    private lateinit var ExistPathTarget : TextField

    @FXML
    override fun initialize(location: URL?, resources: ResourceBundle?) {
        BestTraces.selectionModel.selectedItemProperty().addListener { composand, old, new ->
            renderTrace(new)
        }

        /**
         * It will set to delete edge on double click.
         * */
        fxmlGraphPanel.setEdgeDoubleClickAction {
            Platform.runLater { cleanFounded() }
            if(!BestTraces.items.isEmpty()) clearTraces()
            val road = it.underlyingEdge.element()

            if(graph.containsEdge(road.start, road.end)) {
                graph.removeEdge(road.start, road.end)
                if(readyEdges.contains(road.toString())) readyEdges.remove(road.toString())
                if(readyEdges.contains(road.toStringReverse())) readyEdges.remove(road.toStringReverse())

                fxmlGraphModel.removeEdge(it.underlyingEdge)
            }

            if(graph.getMappableEdges(road.start).isEmpty()) {
                graph.removeVertex(road.start)
                if(readyNodes.contains(road.start)) readyNodes.remove(road.start)

                val vertex = it.underlyingEdge.vertices().find { vertex -> vertex.element() == road.start.name }
                fxmlGraphModel.removeVertex(vertex)
            }

            if(graph.getMappableEdges(road.end).isEmpty()) {
                graph.removeVertex(road.end)
                if(readyNodes.contains(road.end)) readyNodes.remove(road.end)

                val vertex = it.underlyingEdge.vertices().find { vertex -> vertex.element() == road.end.name }
                fxmlGraphModel.removeVertex(vertex)
            }

            fxmlGraphPanel.update()
        }
    }

    @FXML
    fun AddNewDirection() {
        Platform.runLater { cleanFounded() }

        if(!BestTraces.items.isEmpty()) clearTraces()

        try {
            val start = AddPathSource.text.trim()
            val end = AddPathTarget.text.trim()

            if(graph.containsEdge(City(start), City(end))){
                GuiAlerts.roadAlreadyExist(start, end)
                return
            }

            if (!graph.containsVertex(City(start))) graph.addVertex(City(start))
            if (!graph.containsVertex(City(end))) graph.addVertex(City(end))

            val road = Road(AddPathPrice.text.trim().toInt(), City(start), City(end))
            graph.addEdge(road, City(start), City(end))

            Platform.runLater { renderNewDirection(road) }

        } catch (ex: NumberFormatException) {
            GuiAlerts.input()
        }
    }

    @FXML
    fun AddDisabledDirection() {
        Platform.runLater { cleanFounded() }

        if(!BestTraces.items.isEmpty()) clearTraces()

        val start = DisablePathSource.text.trim()
        val end = DisablePathTarget.text.trim()

        if(!graph.containsVertex(City(start))){
            GuiAlerts.notExistingVertex(start, true)
            return
        }

        if(!graph.containsVertex(City(end))){
            GuiAlerts.notExistingVertex(start, false)
            return
        }
        val toDisable = graph.getEdge(City(start), City(end))

        if(toDisable != null){
            toDisable.disable = true
            fxmlGraphPanel.getStylableEdge(toDisable).addStyleClass("edge-disable")
        }
    }

    @FXML
    fun FindOptimalDirection() {
        Platform.runLater { cleanFounded() }

        if(!BestTraces.items.isEmpty()) clearTraces()

        val start = OptimalPathSource.text.trim()
        val end = OptimalPathTarget.text.trim()

        if(!graph.containsVertex(City(start))){
            GuiAlerts.notExistingVertex(start, true)
            return
        }

        if(!graph.containsVertex(City(end))){
            GuiAlerts.notExistingVertex(start, false)
            return
        }

        graph.edges().iterator().forEach {
            COLORS.forEach { color -> Platform.runLater { fxmlGraphPanel.getStylableEdge(it).removeStyleClass(color) }}
        }

        val shortest = search.findAll(City(start), City(end))

        if(shortest.isEmpty()){
            GuiAlerts.noTraceFound(start, end)
            return
        }

        BestTraces.items.clear()
        shortest.forEach { BestTraces.items.add(it) }
        if(!BestTraces.items.isEmpty()) BestTraces.selectionModel.selectFirst()

        if(SaveToFile.isSelected) saveToFile(shortest)
    }

    @FXML
    fun ExistDirection(){
        Platform.runLater { cleanFounded() }

        val target = ExistPathTarget.text.trim()
        val source = ExistPathSource.text.trim()

        if(target.isNotEmpty()){
            if(graph.containsVertex(City(target))){
                Platform.runLater {
                    val stylableVertex = fxmlGraphPanel.getStylableVertex(target)
                    stylableVertex.addStyleClass(FIND_VERTEX_COLOR)
                }
            }
        }
        if(target.isNotEmpty()){
            if(graph.containsVertex(City(source))){
                Platform.runLater {
                    val stylableVertex = fxmlGraphPanel.getStylableVertex(source)
                    stylableVertex.addStyleClass(FIND_VERTEX_COLOR)
                }
            }
        }

        if((target.isNotEmpty()) && (target.isNotEmpty())){
            if(graph.containsEdge(City(target), City(source))){
                Platform.runLater {
                    val stylableEdge = fxmlGraphPanel.getStylableEdge(graph.getEdge(City(source), City(target)))
                    stylableEdge.removeStyleClass("edge")
                    stylableEdge.addStyleClass(FIND_EDGE_COLOR)
                }
            }
        }

        Platform.runLater { fxmlGraphPanel.update() }
    }

    @FXML
    fun LoadGraphFromFile() {
        val fileChooser = FileChooser()
        fileChooser.title = "Open Resource File"
        val file = fileChooser.showOpenDialog(RoadGraphApplication.primaryStage)

        if(file == null || !file.exists()) return

        Platform.runLater { fxmlGraphModel.edges().forEach { fxmlGraphModel.removeEdge(it) } }
        Platform.runLater { fxmlGraphModel.vertices().forEach { fxmlGraphModel.removeVertex(it) } }
        readyEdges.clear()
        readyNodes.clear()
        fxmlGraphPanel.update()
        graph.clean()

        val data = FileGraphService.load(file.absolutePath)
        data.first.forEach {
            if (!graph.containsVertex(it)) graph.addVertex(it)
        }

        data.second.forEach {
            if (!graph.containsVertex(it.start)) graph.addVertex(it.start)
            if (!graph.containsVertex(it.end)) graph.addVertex(it.end)

            graph.addEdge(it, it.start, it.end)
            Platform.runLater { renderNewDirection(it) }
        }
    }

    @FXML
    fun buildQuadTree(){
        val stage = Stage()
        stage.scene = Scene(TreeBuilder.createMainLayout(graph.vertexes().toList()))
        stage.show()
    }

    /**
     * It will clean all founded edges and vertexes.
     * */
    private fun cleanFounded(){
        graph.edges().forEach {
            Platform.runLater {
                val stylableEdge = fxmlGraphPanel.getStylableEdge(it)
                stylableEdge.removeStyleClass(FIND_EDGE_COLOR)
            }
        }
        graph.vertexes().forEach {
            Platform.runLater {
                val stylableEdge = fxmlGraphPanel.getStylableVertex(it.toString())
                stylableEdge.removeStyleClass(FIND_VERTEX_COLOR)
            }
        }
        fxmlGraphPanel.update()
    }

    /**
     * It will save multiple traces to file.
     *
     * @param shortest traces (include alternatives).
     * */
    private fun saveToFile(shortest:MutableList<Queue<Road>>){
        val fileChooser = FileChooser()
        fileChooser.title = "Open Resource File"
        val file = fileChooser.showSaveDialog(RoadGraphApplication.primaryStage) ?: return

        FileGraphService.saveToFile(file, shortest)
    }

    /**
     * It will put color to graph.
     *
     * @param new road to color trace in graph.
     * */
    private fun renderTrace(new: Queue<Road>?) {
        clearTraces()

        new?.forEach { OptimalPaths.text += it.toString() }
        val index = BestTraces.items.indexOf(new)
        Platform.runLater {
            if(index in 0 until BestTraces.items.size)
                BestTraces.items[index].forEach { fxmlGraphPanel.getStylableEdge(it).addStyleClass(COLORS[(index.rem(COLORS.size))]) }
        }
    }

    /**
     * It will clear all traces from current graph.
     * */
    private fun clearTraces() {
        OptimalPaths.clear()

        graph.edges().forEach {
            COLORS.forEach { color ->
                Platform.runLater {
                    val stylableEdge = fxmlGraphPanel.getStylableEdge(it)
                    stylableEdge.removeStyleClass(color)
                }
            }
        }
        fxmlGraphPanel.update()
    }

    /**
     * It will render vertexes or edge of new direction.
     *
     * @param direction to render.
     * */
    private fun renderNewDirection(direction: Road) {
        if (!readyNodes.contains(direction.start.name)) {
            readyNodes.add(direction.start.name)
            fxmlGraphModel.insertVertex(direction.start.name)
        }

        if (!readyNodes.contains(direction.end.name)) {
            readyNodes.add(direction.end.name)
            fxmlGraphModel.insertVertex(direction.end.name)
        }

        if (!readyEdges.contains(direction.toStringReverse())) {
            val edge = fxmlGraphModel.insertEdge(direction.start.name, direction.end.name, direction)
            readyEdges.add(direction.toString())
            fxmlGraphPanel.update()
            Platform.runLater { if(direction.disable) fxmlGraphPanel.getStylableEdge(edge).addStyleClass("edge-disable") }
            fxmlGraphPanel.update()
        }
    }
}

