package cz.upce.fei.adt.tree

import cz.upce.fei.adt.tree.exception.DuplicateValueInTreeException
import cz.upce.fei.adt.tree.exception.MaxLevelOfTreeReached
import cz.upce.fei.adt.tree.map.area.Area
import cz.upce.fei.adt.tree.map.Coordinatable
import cz.upce.fei.adt.tree.map.position.MapPosition
import java.util.*

/**
 * Representation of quad tree with four nodes.
 * */
class MapQuadTree<E : Coordinatable>(var area : Area = Area()) : Tree<E> {
    private companion object {
        /**
         * Delimiter used for spliting the searching area.
         * */
        const val DELIMITER = 2

        /**
         * Maximu level of tree.
         * */
        const val MAX_LEVEL = 10

        /**
         * The coordinate minus value when not included in interval.
         * */
        const val NOT_INCLUDED = 0.00000000001

        /**
         * Modulo to determine last digit of number.
         * */
        const val MODULO = 10

        /**
         * It will calculate area for tree.
         *
         * @param elements to determinate area.
         * */
        fun <E : Coordinatable> calculateArea(elements : List<E>) = Area(
            xMin = calculateMin(elements.first().lat.toInt()),
            xMax = calculateMax(elements.last().lat.toInt()),
            yMin = calculateMin(elements.first().long.toInt()),
            yMax = calculateMax(elements.last().long.toInt()))

        fun calculateMin(number : Int) = when(number % MODULO){
            in (1..9) -> ((number / MODULO) * 10).toDouble()
            else -> number.toDouble()
        }

        fun calculateMax(number : Int) = when(number % MODULO){
            in (1..9) -> ((number / MODULO + 1) * 10).toDouble()
            else -> number.toDouble()
        }
    }

    /**
     * It will initialize area o tree based on provided elements.
     *
     * */
    constructor(elements: List<E>) : this(calculateArea(elements.sorted())) {
        build(elements)
    }

    /**
     * The main root of the tree.
     * */
    var root: Node = MultipleNode(area)
        private set

    /**
     * The number of the lements in the tree.
     * */
    private var length = 0

    /**
     * It will init the tree and build on the given elements.
     *
     * @param elements to be build on that tree.
     * */
    override fun build(elements: List<E>) {
        clean()
        elements.forEach {
            add(it, root)
            length++
        }
    }

    /**
     * It will recursively place element into the tree.
     *
     * @param element to place into new position in tree.
     * @param node node to place element into.
     * @param level optional parameter to determinate level of nodes. It is also used to pick the correct numbers from coordinates.
     * */
    private fun add(element: E, node: Node, level: Int = 0) {
        val toPlace = nodePosition(element, level)
        val index = toPlace.treeIndex

        when (node.leafs[index]) {
            is MultipleNode -> add(element, node.leafs[index], level + 1)
            is EmptyNode -> node.leafs[index] = ValueNode(element, calculateArea(toPlace, node.area))
            is ValueNode<*> -> {
                val backup = node.leafs[index] as ValueNode<E>

                if (backup.emca == element) throw DuplicateValueInTreeException()

                node.leafs[index] = MultipleNode(calculateArea(toPlace, node.area))
                add(backup.emca, node.leafs[index], level + 1)
                add(element, node.leafs[index], level + 1)
            }
        }
    }

    /**
     * It will calculate the are base on previous node area.
     *
     * @param toPlace representing the map area where the new node will be placed.
     * @param previousArea on the previous node to calculate the new child node area location.
     *
     * @return the area representing by the new node.
     * */
    private fun calculateArea(toPlace: MapPosition, previousArea: Area): Area =
        when (toPlace) {
            MapPosition.NORTH_WEST ->
                Area(
                    xMin = previousArea.xMin,
                    xMax = previousArea.xMin / DELIMITER + previousArea.xMax / DELIMITER - NOT_INCLUDED,
                    yMin = previousArea.yMin,
                    yMax = previousArea.yMin / DELIMITER + previousArea.yMax / DELIMITER - NOT_INCLUDED
                )
            MapPosition.NORTH_EAST ->
                Area(
                    xMin = previousArea.xMax / DELIMITER + previousArea.xMin / DELIMITER,
                    xMax = previousArea.xMax,
                    yMin = previousArea.yMin,
                    yMax = previousArea.yMax / DELIMITER + previousArea.yMin / DELIMITER - NOT_INCLUDED
                )
            MapPosition.SOUTH_EAST ->
                Area(
                    xMin = previousArea.xMin / DELIMITER + previousArea.xMax / DELIMITER,
                    xMax = previousArea.xMax,
                    yMin = previousArea.yMin / DELIMITER + previousArea.yMax / DELIMITER,
                    yMax = previousArea.yMax
                )
            MapPosition.SOUTH_WEST ->
                Area(
                    xMin = previousArea.xMin,
                    xMax = previousArea.xMin / DELIMITER + previousArea.xMax / DELIMITER - NOT_INCLUDED,
                    yMin = previousArea.yMin / DELIMITER + previousArea.yMax / DELIMITER,
                    yMax = previousArea.yMax
                )
        }

    override fun contains(element: E): Boolean = contains(element, root)

    /**
     * It will recursively search for element.
     *
     * @param element to searching for.
     * @param node next element to examine.
     * @param level optional parameter to determinate level of searching node.
     *
     * @return if element is containing in the tree or not.
     * */
    private fun contains(element: E, node: Node, level: Int = 0): Boolean =
        when (val next = node.leafs[nodePosition(element, level).treeIndex]) {
            is MultipleNode -> contains(element, next, level + 1)
            is ValueNode<*> -> (next as ValueNode<E>).emca == element
            is EmptyNode -> false
            else -> false
        }


    /**
     * {@inheritDoc}
     */
    override fun all(topLeft: Coordinatable, bottomRight: Coordinatable): MutableList<E> {
        val search = Area(xMin = topLeft.lat, xMax = bottomRight.lat, yMin = topLeft.long, yMax = bottomRight.long)
        val inside = mutableListOf<E>()

        val toExamine: Queue<Node> = LinkedList()
        root.leafs.forEach { toExamine.add(it) }

        while (!toExamine.isEmpty()) {
            val examined = toExamine.poll()
            if (!isInside(search, examined.area)) continue

            when (examined) {
                is MultipleNode -> examined.leafs.forEach { toExamine.add(it) }
                is ValueNode<*> -> {
                    val point = (examined as ValueNode<E>).emca
                    if(search.isInside(point)) inside.add(point)
                }
            }
        }

        return inside
    }

    /**
     * It will check if searching area is inside the examined area.
     *
     * @param search area.
     * @param examined area.
     *
     * @return true/false if the searching area is inside the examined area.
     * */
    private fun isInside(search: Area, examined: Area) : Boolean {
        val inXLeft = search.xMin in (examined.xMin .. examined.xMax)
        val inYLeft = search.yMin in (examined.yMin .. examined.yMax)
        val inXRight = examined.xMin in (search.xMin .. search.xMax)
        val inYRight = examined.yMin in (search.yMin .. search.yMax)

        return (inXLeft || inXRight) && (inYLeft || inYRight)
    }


    /**
     * It will determine the node position in tree.
     *
     * @param point that will be placed in the tree.
     * @param level the level of the tree where the point will be placed.
     *
     * @return the position on the node.
     * */
    fun nodePosition(point: Coordinatable, level: Int = 0): MapPosition {
        if (level == MAX_LEVEL)
            throw MaxLevelOfTreeReached()

        val x = area.xMin + (point.lat / area.xMax)
        val y = area.yMin + (point.long / area.yMax)

        val toBin = toBin(x - x.toInt(), y - y.toInt(), level)
        return when (toBin) {
            MapPosition.NORTH_WEST.bin -> MapPosition.NORTH_WEST
            MapPosition.NORTH_EAST.bin -> MapPosition.NORTH_EAST
            MapPosition.SOUTH_WEST.bin -> MapPosition.SOUTH_WEST
            else -> MapPosition.SOUTH_EAST
        }
    }

    /**
     * Calculate the binary part of the coordinates.
     *
     * @param xDec the x decimal part.
     * @param yDec the y decimal part.
     * @param level the level of the tree where the new point will be placed.
     *
     * @return binary string to new place in the tree.
     * */
    fun toBin(xDec: Double, yDec: Double, level: Int): String {
        var x: Double = xDec
        var y: Double = yDec
        var bin = ""

        (0..level).forEach {
            x *= 2; y *= 2

            if (it == level) {
                bin += if (x >= 1) 1 else 0
                bin += if (y >= 1) 1 else 0
            }
            if (x >= 1.0) x -= 1
            if (y >= 1.0) y -= 1
        }

        return bin
    }

    /**
     * {@inheritDoc}
     */
    override fun clean() {
        root = MultipleNode(area)
        length = 0
    }

    /**
     * Create empty tree based on the new area.
     *
     * @param area new area o tree.
     * */
    private fun clean(area : Area){
        this.area = area
        clean()
    }

    /**
     * {@inheritDoc}
     */
    override fun isEmpty(): Boolean = length == 0

    /**
     * {@inheritDoc}
     */
    override fun size(): Int = length

    /**
     * {@inheritDoc}
     */
    override fun sequence(): Sequence<E> {
        val toAdd: Queue<Node> = LinkedList()
        toAdd.add(root)

        return sequence {
            when (toAdd.peek()) {
                is MultipleNode -> (toAdd.poll() as MultipleNode).leafs.forEach { toAdd.add(it) }
                is ValueNode<*> -> yield((toAdd.poll() as ValueNode<E>).emca)
            }
        }
    }

    /**
     * The abstract of Tree node.
     * */
    interface Node {
        /**
         * The leafs of tree. (Maximum four).
         * */
        val leafs: Array<Node>

        /**
         * Area representing the tree.
         * */
        val area: Area
    }

    /**
     * Indicates that node of tree is empty and value can be placed instead of it.
     *
     * @param area representing by the node.
     * */
    class EmptyNode : Node {
        override val leafs: Array<Node> = arrayOf()
        override val area: Area = Area()
        override fun toString() = "No Value"
    }

    /**
     * Indicates that node is crossing into multiple nodes.
     *
     * @param area representing by the node.
     * */
    class MultipleNode(override val area: Area) : Node {
        override val leafs: Array<Node> = Array(4) { EmptyNode() }
        override fun toString() = "Multiple ${area.toString()}"
    }

    /**
     * Indicates that the node is holding the value
     *
     * @param area representing by the node.
     * */
    class ValueNode<E>(val emca: E, override val area: Area) : Node {
        override val leafs: Array<Node> = arrayOf()
        override fun toString() = "${emca.toString()} $area"
    }
}